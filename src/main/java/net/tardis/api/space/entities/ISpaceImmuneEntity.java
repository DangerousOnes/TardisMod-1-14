package net.tardis.api.space.entities;

public interface ISpaceImmuneEntity {

    boolean shouldTakeSpaceDamage();
}
