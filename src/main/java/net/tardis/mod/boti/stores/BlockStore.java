package net.tardis.mod.boti.stores;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.Direction;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.LightType;
import net.minecraft.world.World;

public class BlockStore {

    private BlockState state = Blocks.AIR.getDefaultState();
    private int lightVal = 0;

    public BlockStore(BlockState state, int lightValue) {
        this.state = state;
        this.lightVal = lightValue;
    }

    public BlockStore(PacketBuffer buf) {
        this.decode(buf);
    }

    public BlockStore(World world, BlockPos pos) {
		this(world.getBlockState(pos), world.getLightFor(LightType.SKY, pos));
	}

	public static BlockStore deserialize(CompoundNBT tag) {
        return new BlockStore(
                NBTUtil.readBlockState(tag.getCompound("state")),
                tag.getInt("light"));
    }

    public BlockState getState() {
        return this.state;
    }

    public int getLight() {
        return this.lightVal;
    }

    public CompoundNBT serialize() {
        CompoundNBT nbt = new CompoundNBT();
        nbt.put("state", NBTUtil.writeBlockState(state));
        nbt.putInt("light", lightVal);
        return nbt;
    }

    public void encode(PacketBuffer buf) {
        buf.writeCompoundTag(NBTUtil.writeBlockState(state));
        buf.writeInt(this.lightVal);

    }

    public void decode(PacketBuffer buf) {
        this.state = NBTUtil.readBlockState(buf.readCompoundTag());
        this.lightVal = buf.readInt();
    }


    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof BlockStore))
            return false;

        BlockStore other = (BlockStore) obj;

        if (this.state.equals(other.state))
            return true;

        return false;

    }

    @Override
    public int hashCode() {
        return state.hashCode();
   }

   public static int packLight(World world, BlockPos pos){
        int light = 0;

        for(Direction dir : Direction.values()){
            light += world.getLight(pos);
        }

        return (int)Math.ceil(light / 6.0);
   }

}
