package net.tardis.mod.blocks.multiblock;

import net.minecraft.util.math.BlockPos;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MultiblockPatterns {

    public static final MultiblockPattern SPINNY_BOI = new MultiblockPattern()
            .addLayer(0, " X ", "XXX", " X ")
            .addLayer(1, " XXX ", "X X", " XXX ");

    public static final MultiblockPattern COMPUTER = new MultiblockPattern().addLayer(1, "X");

    /**
     * Master is located at 0, 0, 0, work around that
     *
     * @author Spectre0987
     */
    public static class MultiblockPattern {

        List<BlockPos> pattern = new ArrayList<>();
        BlockPos master;

        public MultiblockPattern addLayer(int layer, String... lines) {
            int z = -(int) Math.floor(lines.length / 2.0);
            for (String line : lines) {
                int x = -(int) Math.floor(line.length() / 2.0);
                for (char c : line.toCharArray()) {
                    if (c == 'X')
                        pattern.add(new BlockPos(x, layer, z));
                    ++x;
                }
                ++z;
            }
            return this;
        }

        public List<BlockPos> getPositions() {
            return Collections.unmodifiableList(pattern);
        }
    }
}
