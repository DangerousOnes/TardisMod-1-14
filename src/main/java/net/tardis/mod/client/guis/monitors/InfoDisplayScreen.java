package net.tardis.mod.client.guis.monitors;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.tileentities.ConsoleTile;

/**
 * Created by 50ap5ud5
 * on 3 May 2020 @ 10:45:48 pm
 */
public class InfoDisplayScreen extends MonitorScreen {

    public InfoDisplayScreen(IMonitorGui gui, String name) {
        super(gui, name);
    }

    @Override
    protected void init() {
        super.init();
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        TileEntity te = this.minecraft.world.getTileEntity(TardisHelper.TARDIS_POS);
        if (te instanceof ConsoleTile && te != null) {
            ConsoleTile console = (ConsoleTile) te;
            drawCenteredString(matrixStack, this.font, new TranslationTextComponent(Constants.Strings.GUI + "info.title").getString(), this.parent.getMinX() + 100, this.parent.getMaxY() + 10, 0xFFFFFF);
            drawString(matrixStack, this.font, Constants.Translations.LOCATION.getString() + WorldHelper.formatBlockPos(console.getCurrentLocation()), this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 4 - 3), 0xFFFFFF);
            drawString(matrixStack, this.font, Constants.Translations.DIMENSION.getString() + WorldHelper.formatDimName(console.getCurrentDimension()), this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 5 - 3), 0xFFFFFF);
            this.drawString(matrixStack, this.font, Constants.Translations.FACING.getString() + console.getExteriorFacingDirection().getName2().toUpperCase(), this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 6 - 3), 0xFFFFFF);
            drawString(matrixStack, this.font, Constants.Translations.TARGET.getString() + WorldHelper.formatBlockPos(console.getDestinationPosition()), this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 7 - 3), 0xFFFFFF);
            drawString(matrixStack, this.font, Constants.Translations.TARGET_DIM.getString() + WorldHelper.formatDimName(console.getDestinationDimension()), this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 8 - 3), 0xFFFFFF);
            drawString(matrixStack, this.font, Constants.Translations.ARTRON.getString() + Constants.TEXT_FORMAT_NO_DECIMALS.format(console.getArtron()) + "AU", this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 9 - 3), 0xFFFFFF);
            drawString(matrixStack, this.font, Constants.Translations.JOURNEY.getString() + Constants.TEXT_FORMAT_NO_DECIMALS.format(console.getPercentageJourney() * 100.0F) + "%", this.parent.getMinX() + 30, this.parent.getMaxY() + (this.font.FONT_HEIGHT * 10 - 3), 0xFFFFFF);
        }
    }

    @Override
    public boolean isPauseScreen() {
        return false;
    }

}
