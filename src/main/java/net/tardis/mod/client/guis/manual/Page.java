package net.tardis.mod.client.guis.manual;

import com.google.common.collect.Lists;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import org.apache.logging.log4j.Level;

import java.io.InputStreamReader;
import java.util.List;

public class Page {

    private static List<PageSerializer> SERIALIZERS = Lists.newArrayList();

    static{
        SERIALIZERS.add(new NormalPageSerializer());
        SERIALIZERS.add(new CoverPageSerializer());
    }

    public static final int WIDTH = 65, LINES = 11;
    private List<String> lines = Lists.newArrayList();
    private int pageNumber;

    public Page(){}

    public List<String> getLines(){
        return this.lines;
    }

    public void setPageNumber(int page){
        this.pageNumber = page;
    }

    //Returns left over string
    public String parseString(String page){

        FontRenderer font = Minecraft.getInstance().fontRenderer;
        List<String> words = Lists.newArrayList(page.split(" |\\n"));

        this.lines = Lists.newArrayList();
        int currentWidth = 0;

        StringBuilder line = new StringBuilder();
        for(int i = 0; i < words.size(); ++i){
            String word = words.get(i);
            int width = font.getStringWidth(word + " ");
            currentWidth += width;

            //If this word can fit on this line
            if(currentWidth <= WIDTH){
                line.append(word + " ");
            }
            //If we should start a new line
            else{
                this.lines.add(line.toString());
                line = new StringBuilder();
                line.append(word + " ");
                currentWidth = 0;
            }

            if(lines.size() >= LINES - 1){
                StringBuilder build = new StringBuilder();
                for(String s : words.subList(i, words.size())){
                    build.append(s + " ");
                }
                return build.toString();
            }

        }

        return "";
    }

    public int getNumberOfLines(){
        return this.lines.size();
    }

    public int getPageNumber(){
        return this.pageNumber;
    }

    public void render(MatrixStack stack, FontRenderer font, int x, int y, int width, int height){
        int index = 0;
        for(String lines : this.getLines()){
            font.drawString(stack, lines, x, y + (font.FONT_HEIGHT + 2) * index, 0x000000);
            ++index;
        }
    }

    public static List<Page> read(ResourceLocation id){
        try{

            JsonObject root = new JsonParser().parse(new InputStreamReader(Minecraft.getInstance().getResourceManager().getResource(id).getInputStream())).getAsJsonObject();

            String type = root.get("type").getAsString();

            for(PageSerializer serializer : SERIALIZERS){
                if(serializer.match(type)){
                    return serializer.read(root);
                }
            }

            /*
            Page page = new Page();
            String leftovers = page.parseString(root.get("page").getAsString());
            pages.add(page);

            while(!leftovers.isEmpty()){
                Page p = new Page();
                leftovers = p.parseString(leftovers);
                pages.add(p);
            }
            return pages;
            */

        }
        catch(Exception e){
            Tardis.LOGGER.log(Level.INFO, String.format("Exception in manual page %s!", id.toString()));
            Tardis.LOGGER.catching(Level.INFO, e);
        }
        return null;
    }

}
