package net.tardis.mod.client.guis.containers;

import java.util.List;

import com.google.common.collect.Lists;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.client.gui.widget.Widget;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.guis.widgets.ImageButton;
import net.tardis.mod.client.guis.widgets.WaypointButton;
import net.tardis.mod.containers.WaypointBankContainer;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.items.DataCrystalItem;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.WaypointScreenMessage;
import net.tardis.mod.network.packets.WaypointScreenMessage.Type;
import net.tardis.mod.tileentities.WaypointBankTile;

public class WaypointContainerScreen extends ContainerScreen<WaypointBankContainer>{

	public static final ResourceLocation TEXTURE = Helper.createRL("textures/gui/containers/waypoint.png");
	
	public WaypointContainerScreen(WaypointBankContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
		super(screenContainer, inv, titleIn);
	}

	@Override
	protected void drawGuiContainerBackgroundLayer(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) {
		this.renderBackground(matrixStack);
		Minecraft.getInstance().textureManager.bindTexture(TEXTURE);
		this.blit(matrixStack, width / 2 - 176 / 2, height / 2 - 222 / 2, 0, 0, 176, 222);
		
	}
	
	

	@Override
	protected void drawGuiContainerForegroundLayer(MatrixStack matrixStack, int x, int y) {		
	    this.font.drawText(matrixStack, this.playerInventory.getDisplayName(), (float)this.playerInventoryTitleX, (float)this.playerInventoryTitleY + 30, 4210752);
	   
	}

	@Override
	public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		super.render(matrixStack, mouseX, mouseY, partialTicks);
		this.renderHoveredTooltip(matrixStack, mouseX, mouseY);
		if (Helper.isInBounds(mouseX, mouseY, this.guiLeft, this.guiTop - 25, this.guiLeft + 85, this.guiTop + 70)) {
			this.renderTooltip(matrixStack, new TranslationTextComponent("tooltip.tardis.waypoint_bank.tardis_input"), mouseX, mouseY);
		}
		if (Helper.isInBounds(mouseX, mouseY, this.guiLeft + 90, this.guiTop - 25, this.guiLeft + 170, this.guiTop + 70)) {
			this.renderTooltip(matrixStack, new TranslationTextComponent("tooltip.tardis.waypoint_bank.crystal_output"), mouseX, mouseY);
		}
	}

	@Override
	protected void init() {
		super.init();
		this.buttons.clear();
		
		WaypointBankTile tile = this.getContainer().getTile();
		if(tile != null) {
			for(int i = 0; i < tile.getWaypoints().size(); ++i) {
				SpaceTimeCoord coord = tile.getWaypoints().get(i);
				if(!SpaceTimeCoord.UNIVERAL_CENTER.equals(coord)) {
					this.addButton(new WaypointButton(this.width / 2 - 80, this.height / 2 - 105 + (i * 18), 76, 18, new StringTextComponent(coord.getName()), i, true));
				}
			}
		}
		
		if(this.container.getSlot(0).getStack().getItem() instanceof DataCrystalItem) {
			List<SpaceTimeCoord> itemCoords = DataCrystalItem.getStoredWaypoints(this.getContainer().getSlot(0).getStack());
			for(int index = 0; index < itemCoords.size(); ++index) {
				SpaceTimeCoord coord = itemCoords.get(index);
				if(!SpaceTimeCoord.UNIVERAL_CENTER.equals(coord)) {
					this.addButton(new WaypointButton(this.width / 2 + 4, this.height / 2 - 105 + (index * 18), 76, 18, new StringTextComponent(coord.getName()), index, false));
				}
			}
 		}
		
		//Copy to Crystal
		ImageButton copyButton = new ImageButton(width / 2 - 80, height / 2 - 11, 180, 36, 18, 18, TEXTURE);
		copyButton.setPressable(() -> {
			Network.sendToServer(new WaypointScreenMessage(Type.COPY_TO_CRYSTAL, this.container.getTile().getPos(), this.getSelected(true)));
			init();
		});
		this.addButton(copyButton);
		
		//Copy to Bank
		ImageButton bankCopyButton = new ImageButton(width / 2 + 60, height / 2 - 11, 180, 36, 18, 18, TEXTURE);
		bankCopyButton.setPressable(() -> {
			Network.sendToServer(new WaypointScreenMessage(Type.COPY_TO_BANK, this.container.getTile().getPos(), this.getSelected(false)));
			init();
		});
		this.addButton(bankCopyButton);
		
		//Delete from Bank
		ImageButton bankDeleteButton = new ImageButton(width / 2 - 60, height / 2 - 11, 198, 36, 18, 18, TEXTURE);
		bankDeleteButton.setPressable(() -> {
			Network.sendToServer(new WaypointScreenMessage(Type.DELETE_FROM_BANK, this.container.getTile().getPos(), this.getSelected(true)));
			init();
		});
		this.addButton(bankDeleteButton);
		
		//Delete from Bank
		ImageButton deleteButton = new ImageButton(width / 2 + 40, height / 2 - 11, 198, 36, 18, 18, TEXTURE);
		deleteButton.setPressable(() -> {
			Network.sendToServer(new WaypointScreenMessage(Type.DELETE_FROM_CRYSTAL, this.container.getTile().getPos(), this.getSelected(false)));
			init();
		});
		this.addButton(deleteButton);
	}
	
	public List<Integer> getSelected(boolean useBank){
		List<Integer> list = Lists.newArrayList();
		
		for(Widget w : this.buttons) {
			if(w.active && w instanceof WaypointButton) {
				WaypointButton way = (WaypointButton)w;
				if(way.getChecked()) {
					if(useBank && way.getIsFromBank())
						list.add(way.getId());
					else if(!useBank && !way.getIsFromBank())
						list.add(way.getId());
				}
			}
		}
		return list;
	}

	@Override
	public boolean mouseClicked(double mouseX, double mouseY, int p_mouseClicked_5_) {
		for(Widget w : this.buttons) {
			if(Helper.isInBounds((int)mouseX, (int)mouseY, w.x, w.y, w.x + w.getWidth(), w.y + w.getHeight())) {
				if(w instanceof WaypointButton) {
					WaypointButton way = (WaypointButton)w;
					way.setChecked(!way.getChecked());
				}
			}
		}
		return super.mouseClicked(mouseX, mouseY, p_mouseClicked_5_);
	}

}
