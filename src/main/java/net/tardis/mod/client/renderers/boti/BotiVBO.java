package net.tardis.mod.client.renderers.boti;

import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.block.Block;
import net.minecraft.block.BlockRenderType;
import net.minecraft.client.renderer.BufferBuilder;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.RenderTypeLookup;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.client.renderer.vertex.VertexFormat;
import net.minecraft.util.math.vector.Matrix4f;
import org.lwjgl.opengl.GL11;

import java.nio.Buffer;
import java.util.Map;
import java.util.stream.Collectors;

public class BotiVBO {

    private static final VertexFormat FORMAT = DefaultVertexFormats.BLOCK;
    private VertexBuffer[] buffers;
    //Stolen from RegionRenderCacheBuilder
    private final Map<RenderType, BufferBuilder> builders = RenderType.getBlockRenderTypes().stream().collect(Collectors.toMap((renderType) -> {
        return renderType;
    }, (renderType) -> {
        return new BufferBuilder(renderType.getBufferSize());
    }));

    public BotiVBO(){
        this.createBuffers();
    }

    public void createBuffers(){
        buffers = new VertexBuffer[BlockRenderType.values().length];
        for(int i = 0; i < BlockRenderType.values().length; ++i){
            buffers[i] = new VertexBuffer(DefaultVertexFormats.BLOCK);
        }
    }

    public VertexBuffer getBuffer(BlockRenderType type){
        return this.buffers[type.ordinal()];
    }

    public void beginBuffer(BlockRenderType type){
        this.getBuffer(type).bindBuffer();
    }

    public void upload(BlockRenderType type, BufferBuilder bb){
        bb.finishDrawing();
        this.getBuffer(type).upload(bb);
    }

    public void unbind(){
        VertexBuffer.unbindBuffer();
    }

    public void draw(BlockRenderType type, Matrix4f matrix){
        this.beginBuffer(type);
        GlStateManager.vertexPointer(3, GL11.GL_FLOAT, FORMAT.getSize(), 0);
        GlStateManager.colorPointer(4, GL11.GL_UNSIGNED_BYTE, FORMAT.getSize(), FORMAT.getOffset(0));
        GlStateManager.texCoordPointer(2, GL11.GL_FLOAT, FORMAT.getSize(), FORMAT.getOffset(0));

        GlStateManager.enableClientState(GL11.GL_VERTEX_ARRAY);
        GlStateManager.enableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
        GlStateManager.enableClientState(GL11.GL_COLOR_ARRAY);
        this.getBuffer(type).draw(matrix, GL11.GL_QUADS);
        GlStateManager.disableClientState(GL11.GL_VERTEX_ARRAY);
        GlStateManager.disableClientState(GL11.GL_TEXTURE_COORD_ARRAY);
        GlStateManager.disableClientState(GL11.GL_COLOR_ARRAY);
        this.unbind();

    }

    public BufferBuilder getBuilder(RenderType type){
        return this.builders.get(type);
    }
}
