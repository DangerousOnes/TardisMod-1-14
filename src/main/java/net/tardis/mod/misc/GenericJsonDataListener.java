package net.tardis.mod.misc;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import net.minecraft.client.resources.JsonReloadListener;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.profiler.IProfiler;
import net.minecraft.resources.IResourceManager;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.fml.network.PacketDistributor;
import net.minecraftforge.fml.network.simple.SimpleChannel;
import net.minecraftforge.fml.server.ServerLifecycleHooks;

/**
 * Instance of a Data Listener that uses gson
 * Credits: Commoble
 */
public class GenericJsonDataListener<T> extends JsonReloadListener{
	
	protected final String folderName;
	protected Class<T> dataClass;
	protected final Logger logger;
	protected static final Gson STANDARD_GSON = new Gson();
	protected Map<ResourceLocation, T> data = new HashMap<>();
	protected Optional<Runnable> syncDataOnReload = Optional.empty();
	private Function<JsonElement, T> mapper;
	
	/**
	 * @param folderName - the name of the folder we wish to look for json files under
	 * @param dataClass - class used
	 * @param logger
	 * @param gson
	 */
	public GenericJsonDataListener(String folderName, Class<T> dataClass, Logger logger, Gson gson, Function<JsonElement, T> mapper) {
		super(gson, folderName);
		this.folderName = folderName;
		this.dataClass = dataClass;
		this.logger = logger;
		this.mapper = mapper;
	}
	
	public GenericJsonDataListener(String folderName, Class<T> dataClass, Logger logger, Function<JsonElement, T> mapper) {
		this(folderName, dataClass, logger, STANDARD_GSON, mapper);
	}

	@Override
	protected void apply(Map<ResourceLocation, JsonElement> jsons, IResourceManager resourceManagerIn,
	        IProfiler profilerIn) {
		this.logger.info("Beginning loading of data for data loader: {}", this.folderName);
		this.data = this.mapValues(jsons, (this::getJsonAsData));
		this.logger.info("Data loader for {} loaded {} entries", this.folderName, this.data.size());
		
		// hacky server test until we can find a better way to do this
		if (ServerLifecycleHooks.getCurrentServer() != null) {
			// if we're on the server and we are configured to send syncing packets, send syncing packets
			this.syncDataOnReload.ifPresent(Runnable::run);
		}
	}
	
	/**
	 * Converts all the values in a map to new values; the new map uses the same keys as the old map
	 * @param <Key> The type of the maps' keys
	 * @param <In> The type of the input map's values
	 * @param <Out> The type of the output map's values
	 * @param inputs The input map
	 * @param mapper A function that converts the input map's values to the output map's values
	 * @return A map with the same keys as the input map but whose values have been transformed
	 **/
	public Map<ResourceLocation, T> mapValues(Map<ResourceLocation, JsonElement> inputs, Function<JsonElement, T> mapper){
		Map<ResourceLocation, T> newMap = new HashMap<>();
		
		inputs.forEach((key, input) -> {newMap.put(key, mapper.apply(input)); this.logger.info("Added: {}", key.toString());});
		
		return newMap;
	}
	
	protected T getJsonAsData(JsonElement json){
		return this.mapper.apply(json);
	}
	
	/**
	 * Autoscribes a packet to be sent to a player during the PlayerLoginEvent
	 * Called in the main Mod constructor
	 * @param <PACKET>
	 * @param channel - the Network Channel to be used
	 * @param packetFactory - a new instance of a the Packet to be sent
	 * @return
	 */
	public <PACKET> GenericJsonDataListener<T> subscribeAsSyncable(final SimpleChannel channel,
		final Function<Map<ResourceLocation, T>, PACKET> packetFactory){
		MinecraftForge.EVENT_BUS.addListener(this.getLoginListener(channel, packetFactory));
		this.syncDataOnReload = Optional.of(() -> channel.send(PacketDistributor.ALL.noArg(), packetFactory.apply(this.data)));
		return this;
	}
	
	private <PACKET> Consumer<PlayerEvent.PlayerLoggedInEvent> getLoginListener(final SimpleChannel channel,
			final Function<Map<ResourceLocation, T>, PACKET> packetFactory){
		return event -> {
			PlayerEntity player = event.getPlayer();
			if (player instanceof ServerPlayerEntity)
			{
				channel.send(PacketDistributor.PLAYER.with(() -> (ServerPlayerEntity)player), packetFactory.apply(this.data));
			}
		};
	}
	
	public Map<ResourceLocation, T> getData(){
		return this.data;
	}
	/**
	 * Sets the data loader's data to that of the input
	 * @param input
	 */
	public void setData(Map<ResourceLocation, T> input) {
		this.data = input;
	}
	
	

}
