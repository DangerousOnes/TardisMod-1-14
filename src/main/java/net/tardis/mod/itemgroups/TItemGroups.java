package net.tardis.mod.itemgroups;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.items.TItems;

public class TItemGroups {

    public static ItemGroup MAIN = new ItemGroup(Tardis.MODID + ".main") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(TItems.KEY_01.get());
        }
    };

    public static ItemGroup ROUNDELS = new ItemGroup(Tardis.MODID + ".roundels") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(TBlocks.roundel_black_concrete_full.get());
        }
    };

    public static ItemGroup FUTURE = new ItemGroup(Tardis.MODID + ".future") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(TBlocks.steel_grate_solid.get());
        }
    };

    public static ItemGroup MAINTENANCE = new ItemGroup(Tardis.MODID + ".maintenance") {
        @Override
        public ItemStack createIcon() {
            return new ItemStack(TItems.DEMAT_CIRCUIT.get());
        }
    };

}
