package net.tardis.mod.controls;

import java.util.ArrayList;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.fml.server.ServerLifecycleHooks;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.ConsoleUpdateMessage;
import net.tardis.mod.network.packets.ConsoleUpdateMessage.DataTypes;
import net.tardis.mod.network.packets.console.DimensionData;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class DimensionControl extends BaseControl {
	
	private static final String MESSAGE = "message.tardis.control.dimchange";
	private ArrayList<ServerWorld> dimList = new ArrayList<ServerWorld>();
	private int index = 0;
	private int avaliableDimensions = 1;
	
	public DimensionControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}
	
	@Override
	public EntitySize getSize() {

		if(getConsole() instanceof NemoConsoleTile) 
			return EntitySize.flexible(4 / 16.0F, 4 / 16.0F);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.225F, 0.225F);

        if(getConsole() instanceof CoralConsoleTile){
        	return EntitySize.flexible(0.125F, 0.125F);
		}
        if(this.getConsole() instanceof HartnellConsoleTile)
        	return EntitySize.flexible(0.125F, 0.125F);
        if (this.getConsole() instanceof XionConsoleTile)
        	return EntitySize.flexible(0.1875F, 0.1875F);
        
        if(this.getConsole() instanceof ToyotaConsoleTile)
        	return EntitySize.flexible(0.3125F, 0.3125F);
        
        if(this.getConsole() instanceof NeutronConsoleTile)
        	return EntitySize.flexible(0.25F, 0.25F);
        
        if(this.getConsole() instanceof KeltConsoleTile)
        	return EntitySize.flexible(0.25F, 0.25F);
         
		return EntitySize.flexible(6 / 16.0F, 2 / 16.0F);
	}
	

	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(0, 12 / 16.0, 8 / 16.0);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(0.004546756986454792, 0.5499999970197678, 0.43130290108982927);


        if(getConsole() instanceof  CoralConsoleTile){
        	return new Vector3d(-0.1918160605288799, 0.28125, 0.85162353648727);
		}
        
        if(this.getConsole() instanceof HartnellConsoleTile)
        	return new Vector3d(0.605, 0.469, -0.516);
        
        if(this.getConsole() instanceof ToyotaConsoleTile)
        	return new Vector3d(-0.557, 0.438, -0.313);
        
        if(this.getConsole() instanceof XionConsoleTile)
        	return new Vector3d(-0.4978632379852379, 0.5, 0.30190849470534653);
        
        if(this.getConsole() instanceof NeutronConsoleTile)
        	return new Vector3d(0.04, 0.40625, 0.8683701375027411);
        
        if(this.getConsole() instanceof KeltConsoleTile)
        	return new Vector3d(-0.9009227420613739, 0.28125, -0.5234101003733683);
        
		return new Vector3d(-0.1 / 16.0, 7 / 16.0, 12 / 16.0);
	}
	
	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		
		if(!console.hasNavCom())
			return false;
		
		return this.doDimChangeAction(console, player);
	}
	
	private boolean doDimChangeAction(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote() && console.getLandTime() <= 0) {
			this.createDimListIfEmpty();
			if(!this.dimList.isEmpty()) {
				this.modIndex(player.isSneaking() ? -1 : 1);
				ServerWorld type = this.dimList.get(index);
				console.setDestination(type.getDimensionKey(), console.getDestinationPosition());
				player.sendStatusMessage(new TranslationTextComponent(MESSAGE).appendSibling(new StringTextComponent(WorldHelper.formatDimName(type.getDimensionKey())).mergeStyle(TextFormatting.LIGHT_PURPLE)), true);
				this.startAnimation();
				
				ConsoleTile tile = this.getConsole();
				if(tile != null)
					Network.sendToTrackingTE(new ConsoleUpdateMessage(DataTypes.DIMENSION_LIST, new DimensionData(this.dimList.size(), this.index)), tile);
			}
			else index = 0;
		}
		return true;
	}
	
	private void modIndex(int i) {
		if(this.index + i >= this.dimList.size()) {
			this.index = 0;
			return;
		}
		if(this.index + i < 0) {
			this.index = this.dimList.size() - 1;
			return;
		}
		this.index += i;
	}
	
	private void createDimListIfEmpty(){
		if(this.dimList.isEmpty()){
			ServerLifecycleHooks.getCurrentServer().getWorlds().forEach(world -> {
				if(WorldHelper.canTravelToDimension(world))
					dimList.add(world);
			});
		}
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SONIC_FAIL.get();
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.DIMENSION.get();
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}
	
	public void setAvaliableDimensions(int dims) {
		this.avaliableDimensions = dims;
	}
	
	public int getAvailableDimensions() {
		return this.avaliableDimensions;
	}
	
	public int getDimListIndex() {
		return this.index;
	}
	
	public void setDimIndex(int index) {
		this.index = index;
	}

}
