package net.tardis.mod.controls;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Direction.Axis;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.BlockPos;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.subsystem.NavComSubsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class AxisControl extends BaseControl{

	Axis axis = Axis.X;
	
	public AxisControl(ControlEntry entry, ConsoleTile console, ControlEntity entity, Axis axis) {
		super(entry, console, entity);
		this.axis = axis;
	}
	
	public Axis getAxis() {
		return this.axis;
	}
	
	public void setAxis(Axis axis) {
		this.axis = axis;
	}
	
	public BlockPos getAddPos(int scale) {
		switch(axis) {
			case X: return new BlockPos(scale, 0, 0);
			case Y: return new BlockPos(0, scale, 0);
			default: return new BlockPos(0, 0, scale);
		}
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote() && console.getLandTime() <= 0) {
            NavComSubsystem navCom = console.getSubsystem(NavComSubsystem.class).orElse(null);
			
			if(navCom == null || !navCom.canBeUsed()) {
				navCom.playDenySound(console);
				return false;
			}

			console.setDestination(console.getDestinationDimension(), console.getDestinationPosition()
				.add(this.getAddPos(player.isSneaking() ? -console.getCoordIncr() : console.getCoordIncr())));
			
			this.setAnimationTicks(10);
		}
		return true;
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.GENERIC_ONE.get();
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_ONE.get();
	}

}
