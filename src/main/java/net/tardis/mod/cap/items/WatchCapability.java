package net.tardis.mod.cap.items;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.misc.ObjectWrapper;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.WatchTimeUpdate;
import net.tardis.mod.tileentities.BrokenExteriorTile;

public class WatchCapability implements IWatch {

    private static final BlockPos RADIUS = new BlockPos(25, 25, 25);
    private BrokenExteriorTile tile;
    private int variant = 0;

    public WatchCapability() {
    }

    @Override
    public void tick(World world, Entity ent, int itemSlot) {
        if (tile == null || tile.isRemoved() || !tile.getPos().withinDistance(ent.getPosition(), 25)) {
            for (BlockPos pos : BlockPos.getAllInBoxMutable(ent.getPosition().subtract(RADIUS), ent.getPosition().add(RADIUS))) {
                if (world.getTileEntity(pos) instanceof BrokenExteriorTile) {
                    tile = (BrokenExteriorTile) world.getTileEntity(pos);
                    break;
                }
            }
        }
        
        if(!world.isRemote && ent instanceof PlayerEntity) {
			this.variant = this.getVariantFromTime((PlayerEntity)ent);
			Network.sendTo(new WatchTimeUpdate(itemSlot, this.variant), (ServerPlayerEntity)ent);
		}
    }

    @Override
    public boolean shouldSpin(Entity ent) {
        return tile != null && !tile.isRemoved() && tile.getPos().withinDistance(ent.getPosition(), 25);
    }
    
    private int getVariantFromTime(PlayerEntity player) {
		final ObjectWrapper<Integer> variant = new ObjectWrapper<>(0);
		if(TardisHelper.isInATardis(player)) {
			TardisHelper.getConsoleInWorld(player.getEntityWorld()).ifPresent(cap -> {
				ServerWorld world = player.world.getServer().getWorld(cap.getCurrentDimension());
				float percent = world.getDayTime() / 24000.0F;
				variant.setValue((int)(Math.ceil(percent * 7.0)) % 7);
			});
		}
		return variant.getValue();
	}
	
	@Override
	public int getVariant() {
		return this.variant;
	}

	@Override
	public void setVariant(int variant) {
		this.variant = variant;
	}


}
