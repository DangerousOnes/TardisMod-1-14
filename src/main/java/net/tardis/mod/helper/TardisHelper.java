package net.tardis.mod.helper;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.apache.logging.log4j.Level;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.server.MinecraftServer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Direction;
import net.minecraft.util.RegistryKey;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.LazyOptional;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.blocks.ConsoleBlock;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.console.misc.PlayerTelepathicConnection;
import net.tardis.mod.tileentities.inventory.PanelInventory;
import net.tardis.mod.world.dimensions.TDimensions;

/*
 * A helper for Tardis - specific things.
 */
public class TardisHelper {
    
    public static final BlockPos TARDIS_POS = new BlockPos(0, 128, 0).toImmutable();
    
    /** <br> 1.16: Compare RegistryKey<DimensionType> because there can be multiple dimensions for one dimension type */
    public static boolean isInATardis(PlayerEntity player) {
        return WorldHelper.areDimensionTypesSame(player.getEntityWorld(), TDimensions.DimensionTypes.TARDIS_TYPE);
    }
    /** Execute back end tasks when a player clicks on a Broken Exterior. 
     * <br> Creates the Dimension, World and setups up various properties.
     * */
    public static ServerWorld setupTardisDim(MinecraftServer server, ConsoleBlock consoleBlock, ConsoleRoom room) {
        ServerWorld tardisWorld = TDimensions.registerOrGetTardisDim(UUID.randomUUID().toString(), server);
        if(tardisWorld != null && !(tardisWorld.getTileEntity(TARDIS_POS) instanceof ConsoleTile)) {
            tardisWorld.setBlockState(TARDIS_POS, consoleBlock.getDefaultState(), 3);
            ConsoleTile console = ((ConsoleTile)tardisWorld.getTileEntity(TARDIS_POS));
            console.onInitialSpawn();
        }
        room.spawnConsoleRoom(tardisWorld, true);
        
        //Randomly contain artron caps
        tardisWorld.getCapability(Capabilities.TARDIS_DATA).ifPresent(cap -> {
            PanelInventory inv = cap.getEngineInventoryForSide(Direction.WEST);
            for(int i = 0; i < inv.getSlots(); ++i) {
            	inv.setStackInSlot(i, tardisWorld.rand.nextDouble() < 0.1 ? new ItemStack(TItems.LEAKY_ARTRON_CAPACITOR.get()) : ItemStack.EMPTY);
            }
        });
        
        return tardisWorld;
    }
    
    /** <br> 1.16: Iterate through Dimension RegistryKey, because there can be more than one Dimension(World) per DimensionType*/
    public static Optional<ConsoleTile> getConsole(MinecraftServer server, ResourceLocation key) {
        for(ServerWorld nextWorld : server.getWorlds()) {
            ResourceLocation name = nextWorld.getDimensionKey().getLocation();
            if(name != null && name.toString().contentEquals(key.toString())) {
                ServerWorld world = server.getWorld(nextWorld.getDimensionKey());
                if(world != null) {
                    TileEntity te = world.getTileEntity(TardisHelper.TARDIS_POS);
                    if(te instanceof ConsoleTile)
                        return Optional.of((ConsoleTile) te);
                }
            }
        }
        return Optional.empty();
    }
    
    /**
     * RegistryKey overload of {@link TardisHelper#getConsole(MinecraftServer, World)}
     * @param server
     * @param worldKey
     * @return
     */
    public static LazyOptional<ConsoleTile> getConsole(MinecraftServer server, RegistryKey<World> worldKey) {
        ServerWorld serverWorld = server.getWorld(worldKey);
        return getConsole(server, serverWorld);
    }
    
    /** <br> 1.16: Use RegistryKey<World> because there can be multiple dimensions for one dimension type */
    public static LazyOptional<ConsoleTile> getConsole(MinecraftServer server, World world) {
        
        try {
            ServerWorld serverWorld = server.getWorld(world.getDimensionKey());
            if(serverWorld != null) {
                TileEntity te = serverWorld.getTileEntity(TARDIS_POS);
                if(te instanceof ConsoleTile)
                    return LazyOptional.of(() -> (ConsoleTile)te);
            }
            return LazyOptional.empty();
        }
        catch(Exception e) {
            Tardis.LOGGER.catching(Level.DEBUG, e);
            return LazyOptional.empty();
        }
    }
    
    public static Optional<ConsoleTile> getConsoleInWorld(World world) {
        TileEntity te = world.getTileEntity(TARDIS_POS);
        if(te instanceof ConsoleTile)
            return Optional.of((ConsoleTile)te);
        return Optional.empty();
    }
    
    public static List<ServerWorld> getTardises(MinecraftServer server){
        List<ServerWorld> list = Lists.newArrayList();
        for(ServerWorld world : server.getWorlds()) {
            if(world.isAreaLoaded(TARDIS_POS, 0) && world.getTileEntity(TARDIS_POS) instanceof ConsoleTile)
                list.add(world);
        }
        return list;
    }
    
    public static Set<RegistryKey<World>> getTardisWorldKeys(MinecraftServer server){
        Set<RegistryKey<World>> set = Sets.newHashSet();
        for(ServerWorld world : server.getWorlds()) {
            if(world.isAreaLoaded(TARDIS_POS, 0) && world.getTileEntity(TARDIS_POS) instanceof ConsoleTile)
                set.add(world.getDimensionKey());
        }
        return set;
    }
    
    public static Set<RegistryKey<World>> getTardisWorldKeysByPlayer(MinecraftServer server, ServerPlayerEntity player){
        Set<RegistryKey<World>> set = Sets.newHashSet();
        for(ServerWorld world : server.getWorlds()) {
            if(world.isAreaLoaded(TARDIS_POS, 0) && world.getTileEntity(TARDIS_POS) instanceof ConsoleTile){
                ConsoleTile console = (ConsoleTile) world.getTileEntity(TARDIS_POS);
                for(PlayerTelepathicConnection connection : console.getEmotionHandler().getConnectedPlayers()){
                    if (connection.getPlayerId().equals(player.getUniqueID())) {
                        set.add(world.getDimensionKey());
                    }
                }
            }
        }
        return set;
    }

    /** Get all Tardis' Tardis names from the Tardis world capability*/
    public static Set<String> getTardisNames(MinecraftServer server){
    	Set<String> set = Sets.newHashSet();
    	for(ServerWorld world : server.getWorlds()) {
            if(world.isAreaLoaded(TARDIS_POS, 0) && world.getTileEntity(TARDIS_POS) instanceof ConsoleTile){
                world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
        	        set.add(data.getTARDISName().replace(" ", ""));
	    	    });
            }
    	}
    	return set;
    }
    
    public static Set<String> getTardisNamesByWorldKey(MinecraftServer server, RegistryKey<World> worldKey){
    	Set<String> set = Sets.newHashSet();
    	for(ServerWorld world : server.getWorlds()) {
            if(world.isAreaLoaded(TARDIS_POS, 0) && world.getTileEntity(TARDIS_POS) instanceof ConsoleTile && world.getDimensionKey() == worldKey){
                world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
        	        set.add(data.getTARDISName().replace(" ", ""));
	    	    });
            }
    	}
    	return set;
    }

    /** Get collection of Tardis Names and their World Keys whose Tardises the player has telepathically connected to*/
    public static Map<RegistryKey<World>, String> getTardisNamesAndWorldsByPlayer(MinecraftServer server, ServerPlayerEntity player){
    	Map<RegistryKey<World>, String> map = new HashMap<>();
    	for(ServerWorld world : server.getWorlds()) {
            if(world.isAreaLoaded(TARDIS_POS, 0) && world.getTileEntity(TARDIS_POS) instanceof ConsoleTile){
                ConsoleTile console = (ConsoleTile) world.getTileEntity(TARDIS_POS);
                for(PlayerTelepathicConnection connection : console.getEmotionHandler().getConnectedPlayers()){
                    if (connection.getPlayerId().equals(player.getUniqueID())) {
                    	world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
                    		map.put(world.getDimensionKey(), data.getTARDISName());
        	    	    });
                    }
                }
            }
    	}
    	return map;
    }

    /** Get collection of Tardis Names whose Tardises the player has telepathically connected to. 
     * <p> Required for Command suggestion providers*/
    public static Collection<String> getTardisNamesForPlayer(MinecraftServer server, ServerPlayerEntity player) {
        Collection<String> collection = TardisHelper.getTardisNamesAndWorldsByPlayer(server, player).values();
	    return collection;
    }

    /**
     * Get a Tardis' RegistryKey by their Tardis Name
     * <p> It is possible for two Tardises to have the same name, so this not foolproof
     * <br> If you want the true unique identifier for a Tardis, use their dimension id
     * @param server
     * @param tardisName
     * @return
     */
    public static Set<RegistryKey<World>> getTardisWorldKeyByName(MinecraftServer server, String tardisName){
    	Set<RegistryKey<World>> set = Sets.newHashSet();
    	for(ServerWorld world : server.getWorlds()) {
    	    world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
    	    	if (data.getTARDISName().replace(" ", "").contentEquals(tardisName)) {
    	    		set.add(world.getDimensionKey());
    	    	}
    	    });
    	}
    	return set;
    }
    
    public static Map<RegistryKey<World>, String> getTardisWorldKeyAndNames(MinecraftServer server){
    	Map<RegistryKey<World>, String> map = new HashMap<>();
    	for(ServerWorld world : server.getWorlds()) {
    		if(world.isAreaLoaded(TARDIS_POS, 0) && world.getTileEntity(TARDIS_POS) instanceof ConsoleTile){
                world.getCapability(Capabilities.TARDIS_DATA).ifPresent(data -> {
        	        map.put(world.getDimensionKey(), data.getTARDISName());
	    	    });
            }
    	}
    	return map;
    }

}