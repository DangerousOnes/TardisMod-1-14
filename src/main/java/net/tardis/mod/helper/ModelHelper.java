package net.tardis.mod.helper;


import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.systems.RenderSystem;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

@OnlyIn(Dist.CLIENT)
public class ModelHelper {

    public static float RENDER_SCALE = 0.0625F;

	/** TODO Find/Create a appropriate RenderType */
    @Deprecated
	public static void renderPartTransparent(MatrixStack stack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha, ModelRenderer... parts) {
		stack.push();
		RenderSystem.enableAlphaTest();
		RenderSystem.enableBlend();
		for(ModelRenderer part : parts) {
			part.render(stack, buffer, packedLight, packedOverlay, 1,1,1, alpha);
		}
		RenderSystem.disableAlphaTest();
		RenderSystem.disableBlend();
		stack.pop();
	}
	
	public static void copyAngle(ModelRenderer parent, ModelRenderer child) {
		child.rotateAngleX = parent.rotateAngleX;
		child.rotateAngleY = parent.rotateAngleY;
		child.rotateAngleZ = parent.rotateAngleZ;
	}

}
