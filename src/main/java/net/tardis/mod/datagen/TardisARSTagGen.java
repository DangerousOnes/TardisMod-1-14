package net.tardis.mod.datagen;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import net.minecraft.block.Block;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.IARS;

public class TardisARSTagGen implements IDataProvider {

	DataGenerator generator;
	
	public TardisARSTagGen(DataGenerator gen){
		this.generator = gen;
	}
	
	@Override
	public void act(DirectoryCache cache) throws IOException {
		List<String> items = Lists.newArrayList();
		
		for (Block blockEntry : ForgeRegistries.BLOCKS) {
			if (blockEntry instanceof IARS) {
				items.add(blockEntry.getRegistryName().toString());
			}
		}
		
		this.generateTable(cache, getPath(this.generator.getOutputFolder(), "ars"), () -> this.serialize(items));
		
	}
	
	public void generateTable(DirectoryCache cache, Path path, Supplier<JsonElement> element) {
        try {
			IDataProvider.save(DataGen.GSON, cache, element.get(), path);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	public static Path getPath(Path base, String path) {
		return base.resolve("data/" + Tardis.MODID + "/tags/" + path + ".json");
	}
	
	public JsonElement serialize(List<String> items) {
		JsonObject root = new JsonObject();
		
		root.add("replace", new JsonPrimitive(false));
		
		JsonArray itemBlock = new JsonArray();
		for(String s : items) {
			itemBlock.add(s);
		}
		root.add("values", itemBlock);
		
		return root;
	}

	@Override
	public String getName() {
		return "TARDIS ARS Item Tag";
	}

}
