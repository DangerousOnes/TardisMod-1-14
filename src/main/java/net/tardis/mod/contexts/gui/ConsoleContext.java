package net.tardis.mod.contexts.gui;

import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.tileentities.ConsoleTile;

public class ConsoleContext extends GuiContext {

	public ConsoleTile tile;
	
	public ConsoleContext(ConsoleTile tile) {
		this.tile = tile;
	}
}
