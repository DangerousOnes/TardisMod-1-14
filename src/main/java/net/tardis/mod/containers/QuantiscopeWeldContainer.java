package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.tileentities.QuantiscopeTile;

public class QuantiscopeWeldContainer extends Container{

	private QuantiscopeTile tile;
	
	protected QuantiscopeWeldContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	
	public QuantiscopeWeldContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		super(TContainers.QUANTISCOPE_WELD.get(), id);
		this.init(inv, (QuantiscopeTile)inv.player.world.getTileEntity(buf.readBlockPos()));
	}
	
	public QuantiscopeWeldContainer(int id, PlayerInventory inv, QuantiscopeTile tile) {
		super(TContainers.QUANTISCOPE_WELD.get(), id);
		this.init(inv, tile);
	}
	
	public void init(PlayerInventory inv, QuantiscopeTile quantiscope) {
		
		tile = quantiscope;
		
		//Parts
		this.addSlot(new SlotItemHandler(quantiscope, 0, 29, 9));
		this.addSlot(new SlotItemHandler(quantiscope, 1, 50, 9));
		this.addSlot(new SlotItemHandler(quantiscope, 2, 16, 29));
		this.addSlot(new SlotItemHandler(quantiscope, 3, 29, 49));
		this.addSlot(new SlotItemHandler(quantiscope, 4, 50, 49));
		
		//Thing to repair
		this.addSlot(new SlotItemHandler(quantiscope, 5, 64, 29));
		//Repaired Item
		this.addSlot(new SlotItemHandler(quantiscope, 6, 120, 29));
		
		//Player Inv
		for(int row = 0; row < 3; ++row) {
	         for(int slot = 0; slot < 9; ++slot) {
	            this.addSlot(new Slot(inv, slot + row * 9 + 9, 8 + slot * 18, 81 + row * 18 + 3));
	         }
        }

	    for(int i1 = 0; i1 < 9; ++i1) {
	       this.addSlot(new Slot(inv, i1, 8 + i1 * 18, 139 + 3));
	    }
	     
	}
	
	public QuantiscopeTile getQuantiscope() {
		return this.tile;
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}
	
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
		ItemStack itemstack = ItemStack.EMPTY;
		final Slot slot = inventorySlots.get(index);
		if ((slot != null) && slot.getHasStack()) {
			final ItemStack itemstack1 = slot.getStack();
			itemstack = itemstack1.copy();

			final int containerSlots = inventorySlots.size() - playerIn.inventory.mainInventory.size();
			if (index < containerSlots) {
				if (!mergeItemStack(itemstack1, containerSlots, inventorySlots.size(), true)) {
					return ItemStack.EMPTY;
				}
			} else if (!mergeItemStack(itemstack1, 0, containerSlots, false)) {
				return ItemStack.EMPTY;
			}
			if (itemstack1.getCount() == 0) {
				slot.putStack(ItemStack.EMPTY);
			} else {
				slot.onSlotChanged();
			}
			if (itemstack1.getCount() == itemstack.getCount()) {
				return ItemStack.EMPTY;
			}
			slot.onTake(playerIn, itemstack1);
		}
		return itemstack;
	}

}
