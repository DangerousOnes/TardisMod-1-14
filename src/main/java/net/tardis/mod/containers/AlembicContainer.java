package net.tardis.mod.containers;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.container.Container;
import net.minecraft.inventory.container.ContainerType;
import net.minecraft.inventory.container.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fluids.capability.CapabilityFluidHandler;
import net.minecraftforge.items.SlotItemHandler;
import net.tardis.mod.containers.slot.SlotItemHandlerFiltered;
import net.tardis.mod.items.TItems;
import net.tardis.mod.tags.TardisItemTags;
import net.tardis.mod.tileentities.AlembicTile;

public class AlembicContainer extends Container{

	AlembicTile tile;
	
	public AlembicContainer(ContainerType<?> type, int id) {
		super(type, id);
	}
	//Client
	public AlembicContainer(int id, PlayerInventory inv, PacketBuffer buf) {
		super(TContainers.ALEMBIC.get(), id);
		this.init(inv, this.tile = (AlembicTile)inv.player.world.getTileEntity(buf.readBlockPos()));
	}
	//Server
	public AlembicContainer(int id, PlayerInventory inv, AlembicTile tile) {
		super(TContainers.ALEMBIC.get(), id);
		this.init(inv, this.tile = tile);
	}
	
	public void init(PlayerInventory player, AlembicTile inv) {
		

		this.addSlot(new SlotItemHandler(inv.getItemStackHandler(), 0, 25, 11));//Insert water
		this.addSlot(new SlotItemHandlerFiltered(inv.getItemStackHandler(), 1, 25, 47, item -> false));//Empty Buckets. Can take, no insert
		this.addSlot(new SlotItemHandlerFiltered(inv.getItemStackHandler(), 2, 62, 11, item -> true)); //Cinnabar
		this.addSlot(new SlotItemHandler(inv.getItemStackHandler(), 3, 62, 47));//Furnace Fuel slot
		this.addSlot(new SlotItemHandlerFiltered(inv.getItemStackHandler(), 4, 140, 11, item -> item.getItem() == Items.GLASS_BOTTLE));
		this.addSlot(new SlotItemHandler(inv.getItemStackHandler(), 5, 140, 47));//Result slot. Can take, no insert
		
		//Player Inv
		for(int row = 0; row < 3; ++row) {
	         for(int slot = 0; slot < 9; ++slot) {
	            this.addSlot(new Slot(player, slot + row * 9 + 9, 8 + slot * 18, 81 + row * 18 + 3));
	         }
        }

	    for(int i1 = 0; i1 < 9; ++i1) {
	       this.addSlot(new Slot(player, i1, 8 + i1 * 18, 139 + 3));
	    }
	}

	@Override
	public boolean canInteractWith(PlayerEntity playerIn) {
		return true;
	}
	
	public AlembicTile getAlembic() {
		return tile;
	}
	
	@Override
	public ItemStack transferStackInSlot(PlayerEntity playerIn, int index) {
		Slot slot = this.getSlot(index);
		if(slot.inventory instanceof PlayerInventory) {
			//Empty bottles
			if(slot.getStack().getItem() == Items.GLASS_BOTTLE) {
				ItemStack stack = this.tile.getItemStackHandler().insertItem(4, slot.getStack().copy(), false);
				slot.putStack(stack);
				return stack;
			}
			//Fuel
			if(slot.getStack().getBurnTime() > 0) {
				ItemStack stack = this.tile.getItemStackHandler().insertItem(3, slot.getStack().copy(), false);
				slot.putStack(stack);
				return stack;
			}
			//Water
			if(slot.getStack().getCapability(CapabilityFluidHandler.FLUID_HANDLER_ITEM_CAPABILITY).orElse(null) != null) {
				ItemStack stack = tile.getItemStackHandler().insertItem(0, slot.getStack().copy(), false);
				slot.putStack(ItemStack.EMPTY);
				return stack;
			}
			//Cinnabar
			if(slot.getStack().getItem().isIn(TardisItemTags.CINNABAR)) {
				ItemStack stack = tile.getItemStackHandler().insertItem(2, slot.getStack().copy(), false);
				slot.putStack(stack);
				return stack;
			}
		}
		else {
			ItemStack stack = slot.getStack().copy();
			if(playerIn.addItemStackToInventory(stack)) {
				slot.putStack(ItemStack.EMPTY);
				return stack;
			}
		}
		return ItemStack.EMPTY;
	}


}
