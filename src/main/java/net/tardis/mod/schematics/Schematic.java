package net.tardis.mod.schematics;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.Util;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.tileentities.ConsoleTile;

public abstract class Schematic extends ForgeRegistryEntry<Schematic>{
	
	protected String translationKey;
	
	public abstract void onConsumedByTARDIS(ConsoleTile tile, PlayerEntity player);
	
	public String getTranslationKey() {
		if (translationKey == null) {
			this.translationKey = Util.makeTranslationKey("schematic", this.getRegistryName());
		}
		return this.translationKey;
	}
	
	public TranslationTextComponent getDisplayName() {
		return new TranslationTextComponent(this.getTranslationKey());
	}
	
}
