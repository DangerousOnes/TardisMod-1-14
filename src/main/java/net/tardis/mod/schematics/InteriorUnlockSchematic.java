package net.tardis.mod.schematics;

import java.util.function.Supplier;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.tileentities.ConsoleTile;

public class InteriorUnlockSchematic extends Schematic{

	private Supplier<ConsoleRoom> interior;
	
	public InteriorUnlockSchematic(Supplier<ConsoleRoom> interior) {
		this.interior = interior;
	}
	
	@Override
	public void onConsumedByTARDIS(ConsoleTile tile, PlayerEntity entity) {
		ConsoleRoom interior = this.interior.get();
		if(!tile.getUnlockManager().getUnlockedConsoleRooms().contains(interior)) {
			tile.getUnlockManager().addConsoleRoom(interior);
			entity.sendStatusMessage(new TranslationTextComponent(Constants.Translations.UNLOCKED_INTERIOR, interior.getDisplayName().getString()), true);
		}
	}
	
	@Override
	public TranslationTextComponent getDisplayName() {
		return new TranslationTextComponent("schematic." + Tardis.MODID + ".interior", this.interior.get().getDisplayName());
	}

}
