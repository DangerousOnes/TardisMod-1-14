package net.tardis.mod.network.packets;

import net.minecraft.client.Minecraft;
import net.minecraft.network.PacketBuffer;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.network.NetworkDirection;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.network.packets.console.*;

import java.util.function.Supplier;

public class ConsoleUpdateMessage {
	
	private DataTypes type;
	private ConsoleData data;
	
	public ConsoleUpdateMessage(DataTypes type, ConsoleData data) {
		this.type = type;
		this.data = data;
	}
	
	public static void encode(ConsoleUpdateMessage mes, PacketBuffer buff) {
		buff.writeInt(mes.type.ordinal());
		
		mes.data.serialize(buff);
	}
	
	public static ConsoleUpdateMessage decode(PacketBuffer buff) {
		DataTypes type = DataTypes.values()[buff.readInt()];
		
		ConsoleData data = type.data.get();
		data.deserialize(buff);
		
		return new ConsoleUpdateMessage(type, data);
	}
	
	public static void handle(ConsoleUpdateMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			TardisHelper.getConsoleInWorld(getWorld(cont.get())).ifPresent(tile -> {
				mes.data.applyToConsole(tile, cont);
			});
		});
		cont.get().setPacketHandled(true);
	}
	
	public static enum DataTypes{
		LAND_CODE(() -> new LandCode("")),
		FUEL(() -> new Fuel(0, 0)),
		SOUND_SCHEME(() -> new SoundSchemeData(null)),
		FORCEFIELD(() -> new ForcefieldData(false)),
		ANTIGRAVS(() -> new AntigravsData(false)),
		CRASH(() -> new CrashData()),
		SUBSYSTEM(() -> new SubsystemData(null, false, false)),
		NAV_COM(() -> new NavComData(false)),
		DIMENSION_LIST(() -> new DimensionData(0, 0)),
		UPGRADE(() -> new UpgradeData(null, true));
		
		Supplier<ConsoleData> data;
		
		DataTypes(Supplier<ConsoleData> data){
			this.data = data;
		}
		
	}
	
	public static World getWorld(NetworkEvent.Context context) {
		
		if(context.getDirection() == NetworkDirection.PLAY_TO_SERVER) {
			return context.getSender().getServerWorld();
		}
		
		return DistExecutor.callWhenOn(Dist.CLIENT, () -> () -> Minecraft.getInstance().world);
		
	}
	

}
