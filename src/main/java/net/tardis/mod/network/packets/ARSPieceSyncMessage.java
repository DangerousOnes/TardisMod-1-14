package net.tardis.mod.network.packets;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

import com.mojang.serialization.Codec;

import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.NBTDynamicOps;
import net.minecraft.network.PacketBuffer;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.ars.ARSPiece;

public class ARSPieceSyncMessage {
    
	private Map<ResourceLocation, ARSPiece> arsPieces = new HashMap<ResourceLocation, ARSPiece>();
	//We use an unboundedMapCodec. However it is limited in that it can only parse objects whose keys can be serialised to a string, such as ResourceLocation
	//E.g. If you used an int as a key, the unboundedMapCodec will not parse it and will error.
	public static final Codec<Map<ResourceLocation, ARSPiece>> MAPPER = Codec.unboundedMap(ResourceLocation.CODEC, ARSPiece.getCodec());
	
	
	public ARSPieceSyncMessage(Map<ResourceLocation, ARSPiece> arsPieces) {
		this.arsPieces.clear(); //Clear the client's list and readd the entries from the server
        this.arsPieces.putAll(arsPieces);
	}

	public static void encode(ARSPieceSyncMessage mes, PacketBuffer buf) {
		buf.writeCompoundTag((CompoundNBT)(MAPPER.encodeStart(NBTDynamicOps.INSTANCE, mes.arsPieces).result().orElse(new CompoundNBT())));
	}
	
	public static ARSPieceSyncMessage decode(PacketBuffer buf) {
		//Parse our Map Codec and send the nbt data over. If there's any errors, populate with default Tardis Mod ARS Pieces
		return new ARSPieceSyncMessage(MAPPER.parse(NBTDynamicOps.INSTANCE, buf.readCompoundTag()).result().orElse(ARSPiece.registerCoreARSPieces()));
	}
	
	public static void handle(ARSPieceSyncMessage mes, Supplier<NetworkEvent.Context> cont) {
		cont.get().enqueueWork(() -> {
			ARSPiece.DATA_LOADER.setData(mes.arsPieces); //Set the ARSPieces' Registry to that of the parsed in list
		});
		cont.get().setPacketHandled(true);
	}

}
