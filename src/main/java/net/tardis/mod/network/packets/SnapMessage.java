package net.tardis.mod.network.packets;

import java.util.function.Supplier;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.SoundCategory;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class SnapMessage {

	public SnapMessage() {}
	
	public static void encode(SnapMessage mes, PacketBuffer buf) {}
	
	public static SnapMessage decode(PacketBuffer buf) {
		return new SnapMessage();
	}
	
	public static void handle(SnapMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			
			
		});
		context.get().setPacketHandled(true);
	}
}
