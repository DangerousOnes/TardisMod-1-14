package net.tardis.mod.tileentities;

public interface ITileExtraData {
	/** 1.17: Look into looking at tickers instead */
	void tick();
	
	default void invalidate() {}

    default void onChunkUnload() {}

}
