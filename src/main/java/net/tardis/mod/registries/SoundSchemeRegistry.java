package net.tardis.mod.registries;

import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.sounds.AbstractSoundScheme;
import net.tardis.mod.sounds.SoundSchemeBase;
import net.tardis.mod.sounds.SoundSchemeJunk;
import net.tardis.mod.sounds.SoundSchemeMaster;
import net.tardis.mod.sounds.SoundSchemeTV;

public class SoundSchemeRegistry {
	
    public static final DeferredRegister<AbstractSoundScheme> SOUND_SCHEMES = DeferredRegister.create(AbstractSoundScheme.class, Tardis.MODID);
	
	public static Supplier<IForgeRegistry<AbstractSoundScheme>> SOUND_SCHEME_REGISTRY = SOUND_SCHEMES.makeRegistry("sound_scheme", () -> new RegistryBuilder<AbstractSoundScheme>().setMaxID(Integer.MAX_VALUE - 1));
	
	public static final RegistryObject<AbstractSoundScheme> BASIC = SOUND_SCHEMES.register("basic", ()-> new SoundSchemeBase());
	public static final RegistryObject<AbstractSoundScheme> TV = SOUND_SCHEMES.register("tv", ()-> new SoundSchemeTV());
	public static final RegistryObject<AbstractSoundScheme> MASTER = SOUND_SCHEMES.register("master", ()-> new SoundSchemeMaster());
	public static final RegistryObject<AbstractSoundScheme> JUNK = SOUND_SCHEMES.register("junk", ()-> new SoundSchemeJunk());
}
