package net.tardis.mod.registries;

import java.util.Random;
import java.util.function.Supplier;

import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.RegistryBuilder;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.hostile.dalek.types.DalekType;
import net.tardis.mod.entity.hostile.dalek.types.RustyType;
import net.tardis.mod.entity.hostile.dalek.types.SpecialType;

@Mod.EventBusSubscriber(modid = Tardis.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
public class DalekTypeRegistry {
	
	public static final DeferredRegister<DalekType> DALEK_TYPES = DeferredRegister.create(DalekType.class, Tardis.MODID);

    public static Supplier<IForgeRegistry<DalekType>> DALEK_TYPE_REGISTRY = DALEK_TYPES.makeRegistry("dalek_type", () -> new RegistryBuilder<DalekType>().setMaxID(Integer.MAX_VALUE - 1));
    

    public final static RegistryObject<DalekType> DEFAULT = DALEK_TYPES.register("dalek_default", () -> new DalekType().addVarient(new String[]{"red", "blue", "grey", "black", "redsup", "skarro"}));
    public final static RegistryObject<DalekType> SPECIAL = DALEK_TYPES.register("dalek_spec", () -> new SpecialType().addVarient(new String[]{"specweap"}));
    public final static RegistryObject<DalekType> RUSTY = DALEK_TYPES.register("dalek_rusty", () -> new RustyType().addVarient(new String[]{"rusty", "rusty2", "rusty3"}));
    

    public static DalekType getRandom(Random random) {
        DalekType[] entries = DALEK_TYPE_REGISTRY.get().getValues().toArray(new DalekType[0]);
        return entries[random.nextInt(entries.length)];
    }


}
