package net.tardis.mod.missions;

import java.util.List;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.tardis.mod.cap.ITickableMission;

public abstract class KillMission extends MiniMission implements ITickableMission{

	public KillMission(MiniMissionType type, World world, BlockPos pos, int range) {
		super(type, world, pos, range);
	}
	
	public void onKill(Entity entity) {
		if(!entity.world.isRemote && entity.getType() == this.getEntityType()) {
			if(this.shouldKillAdvance()) {
				this.advanceStage();
			}
		}
	}
	
	public int getValidEntitiesInArea() {
		List<Entity> entities = this.getWorld().getEntitiesWithinAABB(Entity.class, this.getMissionBB());
		entities.removeIf(ent -> ent.getType() != this.getEntityType());
		return entities.size();
	}
	
	public abstract EntityType<?> getEntityType();
	public abstract boolean shouldKillAdvance();
	public abstract boolean shouldSpawnMore();
	public abstract boolean canSpawnHere(World world, BlockPos pos);
	
	public void onSpawnEntity(Entity ent) {}

	@Override
	public void tick(World world) {
		if(!world.isRemote() && this.shouldSpawnMore() && !this.isComplete()) {
			for(PlayerEntity player : world.getEntitiesWithinAABB(PlayerEntity.class, this.getMissionBB())) {
				for(int i = 0; i < 64; ++i) {
					BlockPos pos = this.getValidSpawnPos(world, player);
					if(!pos.equals(BlockPos.ZERO) && this.canSpawnHere(getWorld(), pos)) {
						if(this.shouldSpawnMore()) {
							Entity ent = this.getEntityType().create(world);
							ent.setPosition(pos.getX() + 0.5, pos.getY() + 1, pos.getZ() + 0.5);
							this.onSpawnEntity(ent);
							world.addEntity(ent);
						}
						break;
					}
				}
			}
		}
	}
	
	private BlockPos getValidSpawnPos(World world, PlayerEntity player) {
		
		int minRad = 10;
		int maxRad = 30;
		
		int x = ((int)player.getPosX()) + minRad + player.getRNG().nextInt(maxRad - minRad);
		int y = ((int)player.getPosY());
		int z = ((int)player.getPosZ()) + minRad + player.getRNG().nextInt(maxRad - minRad);
		
		BlockPos pos = new BlockPos(x, y, z);
		if(world.getBlockState(pos).isAir()) {
			for(int ny = (y - minRad); ny < world.getHeight(); ++ny) {
				//If there is a solid block above this one
				if(world.getBlockState(new BlockPos(x, ny, z)).isSolid())
					return pos;
			}
			return pos;
		}
		
		return BlockPos.ZERO;
		
	}
	
}
